package ru.aston.fedotov.techtest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionModel {
    private Integer id;
    private BigDecimal sum;
    private LocalDateTime createdAT;
    private Integer accountId;
}
