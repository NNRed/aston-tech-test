package ru.aston.fedotov.techtest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.aston.fedotov.techtest.data.entity.Transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountModel {
    private Integer id;
    private String beneficiary;
    private Integer pin;
    private BigDecimal balance;
    private Integer accountNumber;
    private List<Transaction> transactions = new ArrayList<>();

    public AccountModel(String beneficiary, Integer pin, BigDecimal balance) {
        this.beneficiary = beneficiary;
        this.pin = pin;
        this.balance = balance;
    }
}
