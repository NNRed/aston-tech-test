package ru.aston.fedotov.techtest.util;

public class TransactionExceptionResponse {
    private final String message;

    public TransactionExceptionResponse(String message) {
        this.message = message;
    }
}
