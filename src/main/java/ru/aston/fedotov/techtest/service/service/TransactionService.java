package ru.aston.fedotov.techtest.service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aston.fedotov.techtest.data.entity.Account;
import ru.aston.fedotov.techtest.data.entity.Transaction;
import ru.aston.fedotov.techtest.data.repository.AccountRepository;
import ru.aston.fedotov.techtest.data.repository.TransactionRepository;
import ru.aston.fedotov.techtest.exception.AccountNotFoundException;
import ru.aston.fedotov.techtest.exception.InvalidPinException;
import ru.aston.fedotov.techtest.model.TransactionModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Transactional
public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;
    private final Converter<Transaction, TransactionModel> converter;

    public TransactionModel deposit(BigDecimal sum, Integer id) {
        final Account account = accountRepository.findById(id).orElseThrow(AccountNotFoundException::new);
        account.setBalance(account.getBalance().add(sum));
        return converter.convert(transactionRepository.saveAndFlush(new Transaction(sum, LocalDateTime.now(), account)));
    }

    public TransactionModel withdraw(BigDecimal sum, Integer pin, Integer id) {
        final Account account = accountRepository.findById(id).orElseThrow(AccountNotFoundException::new);
        if (account.getPinCode() == pin) {
            account.setBalance(account.getBalance().subtract(sum));
            return converter.convert(transactionRepository.saveAndFlush(new Transaction(sum, LocalDateTime.now(), account)));
        }
        else {
            throw new InvalidPinException();
        }
    }
}
