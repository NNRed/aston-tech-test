package ru.aston.fedotov.techtest.service.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.aston.fedotov.techtest.data.entity.Transaction;
import ru.aston.fedotov.techtest.model.TransactionModel;

@Component
public class TransactionToTransactionModelConverter implements Converter<Transaction, TransactionModel> {
    @Override
    public TransactionModel convert(Transaction source) {
        return new TransactionModel(source.getId(),
                source.getSum(),
                source.getCreatedAt(),
                source.getAccount().getId());
    }
}
