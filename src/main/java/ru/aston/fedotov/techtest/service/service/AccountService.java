package ru.aston.fedotov.techtest.service.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aston.fedotov.techtest.data.repository.AccountRepository;
import ru.aston.fedotov.techtest.data.entity.Account;
import ru.aston.fedotov.techtest.model.AccountModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class AccountService {
    private final AccountRepository repository;
    private final Converter<Account, AccountModel> converter;

    public List<AccountModel> getAll() {
        return repository.findAll().stream().map(converter::convert).collect(Collectors.toList());
    }

    public AccountModel create(String beneficiary, Integer pin, BigDecimal balance) {
        return converter.convert(repository.saveAndFlush(new Account(beneficiary, pin, balance, random())));
    }

    private int random() {
        return new Random().nextInt(100_000_000);
    }

    public AccountModel report(Integer id) {
        return repository.findById(id).map(converter::convert).orElse(null);
    }
}
