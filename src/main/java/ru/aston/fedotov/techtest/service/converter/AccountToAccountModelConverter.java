package ru.aston.fedotov.techtest.service.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.aston.fedotov.techtest.data.entity.Account;
import ru.aston.fedotov.techtest.model.AccountModel;

@Component
public class AccountToAccountModelConverter implements Converter<Account, AccountModel> {
    @Override
    public AccountModel convert(Account source) {
        return new AccountModel(source.getId(),
                source.getBeneficiary(),
                source.getPinCode(),
                source.getBalance(),
                source.getAccountNumber(),
                source.getTransactions());
    }
}
