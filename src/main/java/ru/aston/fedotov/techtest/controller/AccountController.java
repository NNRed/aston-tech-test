package ru.aston.fedotov.techtest.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.aston.fedotov.techtest.model.AccountModel;
import ru.aston.fedotov.techtest.service.service.AccountService;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@Tag(name = "Account", description = "Account APi")
public class AccountController {
    private final AccountService service;

    @GetMapping
    @Operation(description = "Get all accounts.", tags = "account")
    @ApiResponse(responseCode = "200")
    public ResponseEntity<List<AccountModel>> getAll() {
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping("/create")
    @Operation(description = "Create new account.")
    @ApiResponse(responseCode = "200", description = "Create new account",
            content = {@Content(mediaType = "application/json")})
    public ResponseEntity<AccountModel> create(@RequestBody AccountModel account) {
        return ResponseEntity.ok(service.create(account.getBeneficiary(), account.getPin(), account.getBalance()));
    }

    @GetMapping("/report/{id}")
    public ResponseEntity<AccountModel> report(@PathVariable Integer id){
        return ResponseEntity.ok(service.report(id));
    }
}
