package ru.aston.fedotov.techtest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.aston.fedotov.techtest.exception.AccountNotFoundException;
import ru.aston.fedotov.techtest.exception.InvalidPinException;
import ru.aston.fedotov.techtest.model.TransactionModel;
import ru.aston.fedotov.techtest.service.service.TransactionService;
import ru.aston.fedotov.techtest.util.TransactionExceptionResponse;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/transactions")
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService service;

    @PostMapping("/deposit/{id}")
    public ResponseEntity<TransactionModel> deposit(@PathVariable Integer id, @RequestBody TransactionModel model) {
        return ResponseEntity.ok(service.deposit(model.getSum(), id));
    }

    @PostMapping("/withdraw/{id}")
    public ResponseEntity<TransactionModel> withdraw(@PathVariable Integer id, @RequestBody TransactionModel model, @RequestBody Integer pin) {
        return ResponseEntity.ok(service.withdraw(model.getSum(), pin, id));
    }

    @ExceptionHandler
    public ResponseEntity<TransactionExceptionResponse> exception(AccountNotFoundException e){
        return  new ResponseEntity<>(new TransactionExceptionResponse("Account doesn't exist"), NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<TransactionExceptionResponse> exception(InvalidPinException e){
        return new ResponseEntity<>(new TransactionExceptionResponse("Invalid pin"), BAD_REQUEST);
    }
}
