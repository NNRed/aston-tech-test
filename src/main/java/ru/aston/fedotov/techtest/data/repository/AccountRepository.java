package ru.aston.fedotov.techtest.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.aston.fedotov.techtest.data.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
}
