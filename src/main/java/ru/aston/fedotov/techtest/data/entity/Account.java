package ru.aston.fedotov.techtest.data.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "account")
@Getter
@Setter
@ToString(exclude = "transactions")
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "beneficiary")
    private String beneficiary;

    @Column(name = "pin_code")
    private Integer pinCode;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "account_number")
    private Integer accountNumber;

    @OneToMany(mappedBy = "account")
    private List<Transaction> transactions = new ArrayList<>();

    public Account(String beneficiary, Integer pinCode, BigDecimal balance, Integer accountNumber) {
        this.beneficiary = beneficiary;
        this.pinCode = pinCode;
        this.balance = balance;
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) && Objects.equals(beneficiary, account.beneficiary) && Objects.equals(pinCode, account.pinCode) && Objects.equals(balance, account.balance) && Objects.equals(accountNumber, account.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, beneficiary, pinCode, balance, accountNumber);
    }
}
