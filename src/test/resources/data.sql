create table account
(
    id             int primary key auto_increment,
    beneficiary    varchar(100) not null,
    balance        decimal      not null check (balance > 0),
    pin_code       int          not null check (pin_code > 999 and pin_code < 10000),
    account_number bigint       not null
);

create table transaction
(
    id         int primary key auto_increment,
    sum        decimal check (sum > 0),
    created_at timestamp not null,
    account_id int references account (id)
);

insert into account(beneficiary, pin_code, account_number)
values ('First', 1234, 90909090);
insert into account(beneficiary, pin_code, account_number)
values ('Second', 1234, 90909090);
insert into account(beneficiary, pin_code, account_number)
values ('Third', 1234, 90909090);