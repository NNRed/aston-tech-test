package ru.aston.fedotov.techtest.service.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.aston.fedotov.techtest.data.entity.Account;
import ru.aston.fedotov.techtest.data.repository.AccountRepository;
import ru.aston.fedotov.techtest.model.AccountModel;
import ru.aston.fedotov.techtest.service.converter.AccountToAccountModelConverter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
class AccountServiceTest {
    @Autowired
    private AccountService subj;

    @MockBean
    private AccountRepository repository;

    @MockBean
    private AccountToAccountModelConverter converter;

    Account account;
    List<Account> accounts;

    @BeforeEach
    public void setUp() {
        account = new Account(1,
                "First",
                1234,
                BigDecimal.valueOf(1000L),
                11111111,
                Collections.emptyList());

        accounts = new ArrayList<>();
        accounts.add(account);
    }

    @Test
    void shouldGetAllAccounts() {
        when(repository.findAll()).thenReturn(accounts);
        final List<AccountModel> all = subj.getAll();

        assertTrue(all.size() == 1);
        verify(converter, times(1)).convert(account);
    }

//    @Test
//    void accountsEmptyIfNoAccounts(){
//
//    }
}