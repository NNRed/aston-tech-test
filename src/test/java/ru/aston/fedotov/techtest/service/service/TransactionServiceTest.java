package ru.aston.fedotov.techtest.service.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.aston.fedotov.techtest.data.repository.AccountRepository;
import ru.aston.fedotov.techtest.data.repository.TransactionRepository;
import ru.aston.fedotov.techtest.service.converter.AccountToAccountModelConverter;

import static org.junit.jupiter.api.Assertions.*;

class TransactionServiceTest {
    @Autowired
    private TransactionService subj;

    @MockBean
    private TransactionRepository transactionRepository;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private AccountToAccountModelConverter converter;

    @BeforeEach
    void setUp() {
    }

    @Test
    void deposit() {
    }

    @Test
    void withdraw() {
    }
}